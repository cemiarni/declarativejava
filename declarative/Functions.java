package declarative;

public final class Functions{

  public static final List NIL=new List();
  public static final int TRUE=1;
  public static final int FALSE=0;
  
  public static List cons(int a, List b){
  
      return new List(a,b);
  
  }

  public static int head(List list){
  
      return list.number;
  
  }
  
  public static List tail(List list){
  
      return list.pointer;
  
  }
  
  public static int car(List list){
  
      return head(list);
  
  }
  
  public static List cdr(List list){
  
      return tail(list);
  
  }
  
  
  public static int writeln(List list){
  
      write(list);
      System.out.println();
      
      return 0;
  
  }
  
  public static int write(List list){
  
    List l=list;
    
    if(list==NIL) System.out.print("NIL");
    else{
	System.out.print("(");
	
	while(l.pointer!=NIL){
	
	    System.out.print(String.format("%d ",l.number));
	    l=l.pointer;
	
	}
	
	System.out.print(l.number);
	
	System.out.print(")");
    }
    
    return 0;
  
  }
  
  public static int write(int number){
  
      System.out.print(number);
      
      return 0;
  
  }
  
  public static int writeln(int number){
  
      System.out.println(number);
      
      return 0;
  
  }
  
  public static List append(List list1, List list2){
  
      List l1=copy(list1);
      List l2=copy(list2);
      
      if(l1==NIL) return l2;
      
      List temp=l1;
      
      while(temp.pointer!=NIL)
      
	temp=temp.pointer;
	
      temp.pointer=l2;
      
      return l1;
  
  }
  
  public static List reverse(List list){

      return revapp(list,NIL);
  
  }
 
  public static List revapp(List list1, List list2){
  
      List retv=copy(list2);
      while(list1!=NIL){
      
	  retv=cons(list1.number,retv);
	  list1=list1.pointer;
      
      }
      
      return retv;
  
  }
  
  public static int ismember(List list, int number){

      List l=list;
      
      while(l!=NIL){
      
	  if(l.number==number) return TRUE;
      
	  l=l.pointer;
      
      }
      
      return FALSE;
  
  }
  
  public static int forall(Fun1 func, List list){
  
      List l=list;
      
      if(l==NIL) return FALSE;
      
      while(l!=NIL){
      
	  if(func.f(l.number)==FALSE) return FALSE;
      
	  l=l.pointer;
      
      }
      
      return TRUE;
  
  }
  
  public static List map(Fun1 func, List list){
  
      List l=copy(list);
      List retv=l;
      
      while(l!=NIL){
      
	  l.number=func.f(l.number);
      
	  l=l.pointer;
      
      }
      
      return retv;
  
  }
  
  public static List filter(Fun1 func, List list){
  
      List retv=NIL;
  
      while(list!=NIL){
      
	if(func.f(list.number)!=FALSE) retv=new List(list.number,retv);
	
	list=list.pointer;
      
      }
      
      return retv;
  
  }
  
//   public static int foldl(Fun2 func, List list){
//   
//   
//   }
//   
//   public static int foldr(Fun2 func, List list){
//   
//   
//   }

  private static List copy(List list){
  
      List l=list;
      List retv=NIL;
      
      while(l!=NIL){
      
	  retv=new List(l.number, retv);
	  
	  l=l.pointer;
      
      }
      
      l=retv;
      retv=NIL;
      while(l!=NIL){
      
	  retv=new List(l.number, retv);
	  
	  l=l.pointer;
      
      }
      
      return retv;
  
  }

}
