package test;

import static declarative.Functions.*;
import declarative.List;

public class Test{

  private static List generateList(final int pcs){
  
      if(pcs==0) return NIL;
      return cons(pcs,generateList(pcs-1));
 
  }

  public static void main(String[] args){
  
     final List list1=generateList(10);
     final List list2=generateList(10);
     
     writeln(list1);
     writeln(list2);
     
     writeln(append(list1, list2));
     
     writeln(forall((int n) -> (n<11 && n>0)?TRUE:FALSE, list1));
     writeln(forall((int n) -> (n<10 && n>0)?TRUE:FALSE, list1));
     writeln(ismember(list1,2));
     writeln(ismember(list1,0));
     
     writeln(map((int n) -> n*n,list1));
     
     writeln(filter((int n) -> (n<8 && n>2)?TRUE:FALSE,list1));
     writeln(reverse(list1));
     writeln(revapp(list1,list2));

  }
  
}
